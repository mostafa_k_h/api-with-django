from django.urls import path
from . import views

urlpatterns = [
    path('all_posts/',views.all_posts,name='all_posts'),#http://127.0.0.1:8000/api/blog/all_posts/
    path('insert_posts/',views.insert_posts,name='insert_posts'),#http://127.0.0.1:8000/api/blog/insert_posts/
    path('delete_posts/',views.delete_posts,name='delete_posts'),#http://127.0.0.1:8000/api/blog/delete_posts/
    path('update_posts/',views.update_posts,name='update_posts'),#http://127.0.0.1:8000/api/blog/delete_posts/
]