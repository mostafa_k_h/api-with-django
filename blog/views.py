from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from .models import Author,Post
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

@csrf_exempt
#Create an API to view posts
def all_posts(request):
   all_posts = Post.objects.all()
   all_posts_serialized = serializers.serialize('json',all_posts)
   all_posts_json = json.loads(all_posts_serialized)
   data = json.dumps(all_posts_json)
   return HttpResponse(data)


@csrf_exempt
#Create API for add post  
def insert_posts(request):
    try:
        title = request.POST.get('title')
        description = request.POST.get('description')
        author = Author.objects.get('pk=1')
    

        post_instace = Post()
        post_instace.title = title
        post_instace.description = description
        post_instace.author = author
        post_instace.save()

        return HttpResponse('200')
    
    except:
        return HttpResponse('NOT OK')

@csrf_exempt
#Create API for delete post  
def delete_posts(request):
    try:
        id = request.POST.get('id')
        Post.objects.filter(id=id).delete()
        return HttpResponse('200')
    
    except:
        return HttpResponse('NOT OK')

@csrf_exempt
#Create API for delete post  
def update_posts(request):
    try:
        id = request.POST.get('id')
        Post.objects.filter(id=id).delete()
        return HttpResponse('200')
    
    except:
        return HttpResponse('NOT OK')




@csrf_exempt
#Create API for update post  
def update_posts(request):
    try:
        id = request.POST.get('id')
        title = request.POST.get('title')
        description = request.POST.get('description')
        Post.objects.filter(id=id).update(title=title,description=description)
        return HttpResponse('200')
    
    except:
        return HttpResponse('NOT OK')

